Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-09-24T14:56:54+02:00

====== Interface et API ======

Les actions de base de gestion des organisation, groupes et jeux de données sont accessible à travers l'interface de CKAN.
Le sysadmin peut gérer / créer les organisations ainsi que toutes les autres actions
	{{./pasted_image004.png}}{{./pasted_image007.png}}

Le sysadmin peut gérer / créer les groupes
	{{./pasted_image005.png}}{{./pasted_image008.png}}

L'administrateur d'une organisation peut ajouter des membres  à une organisation et leur affecter les privilèges:
	* administrateur
	* membres
	* éditeur
	{{./pasted_image010.png}}{{./pasted_image013.png}}

Les administrateurs et les éditeurs d'une organisation peuvent ajouter/modifier et supprimer les jeux de données, rendre public/privé un jeu de données
	{{./pasted_image009.png}}{{./pasted_image008.png}}{{./pasted_image011.png}}

Les administrateurs et les éditeurs d'une organisation peuvent éditer les métadonnées et ajouter/editer les ressources constitutives d'un jeu de données
	{{./pasted_image014.png}}{{./pasted_image020.png}}
Les administrateur et les éditeurs d'une organisation peuvent ajouter un jeu de données à un groupe 
	{{./pasted_image017.png}}

Les administrateurs, les éditeurs et les membres d'une organisation peuvent s'abonner et se désabonner aux flux d'activité d'un jeu de données
	{{./pasted_image018.png}}{{./pasted_image019.png}}


===== L'ensemble de ces action sont pilotables à partir de l'API CKAN =====
L’API  de CKAN  de type RPC expose toutes les fonctionnalités principales de CKAN aux clients d’API. Toutes les fonctionnalités principales d’un site Web de CKAN (tout ce que vous pouvez faire avec l’interface Web et plus encore) peuvent être utilisées par un code externe qui appelle l’API CKAN. 
Par exemple, en utilisant l'API CKAN, votre application peut:
	* Obtenir des listes au format JSON des jeux de données, groupes ou autres objets CKAN d’un site
	* Obtenir une représentation JSON complète d'un ensemble de données, d'une ressource ou d'un autre objet
	* Créer, mettre à jour et supprimer des ensembles de données, des ressources et d'autres objets
	* Obtenir un flux d'activité de jeux de données récemment modifiés sur un site

Vous pouvez ajouter des jeux de données à l’aide de l’interface Web de CKAN, mais lors de l’importation de nombreux jeux de données, il est généralement plus efficace d’automatiser le processus. 


